from flask import Flask
from flask import Flask, render_template, redirect, url_for,request

app = Flask(__name__)
#flask is micro framework

@app.route('/')
def hello_world():
    return "hello world"

@app.route('/products')
def products():
    return 'List of all products'

@app.route('/admin')
def admin():
    return 'admin'

@app.route('/users')
def users():
    return 'users-2'

if __name__ == "__main__":
    app.run()
